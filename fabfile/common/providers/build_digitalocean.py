import logging
import time

import digitalocean
from fabric.api import env, task, local


@task
def prepare():
    """Prepare Digital Ocean specific environment"""
    env.buildparams['name'] = env.buildparams['hostname']
    env.buildparams['region'] = 'sfo1'
    env.buildparams['image'] = 'ubuntu-16-04-x64'
    env.buildparams['size_slug'] = '512mb'
    env.buildparams['backups'] = False

    env.buildparams['root_user'] = 'root'
    env.buildparams['user'] = 'ubuntu'

    with open(env.buildparams['token_path'], 'r') as token_file:
        token = token_file.readline().strip('\n')
    env.buildparams['token'] = token

    manager = digitalocean.Manager(token=token)
    ssh_keys = manager.get_all_sshkeys()
    env.buildparams['ssh_keys'] = ssh_keys


@task
def create_server():
    """Create digital ocean server"""
    manager = digitalocean.Manager(token=env.buildparams['token'])
    droplet = digitalocean.Droplet(**env.buildparams)

    logging.info("""Creating server:
     - Name: {name}
     - Region: {region}
     - Image: {image}
     - Size: {size_slug}
     - Private Networking: {private_networking}
     """.format(**env.buildparams))
    droplet.create()

    env.buildparams['instance_id'] = droplet.id

    # Get IP address
    for i in range(10):
        droplet = manager.get_droplet(droplet.id)
        if droplet.ip_address is None:
            time.sleep(5)
        else:
            env.buildparams['ip_address'] = droplet.ip_address
            break

