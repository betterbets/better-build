# import logging
# import time
# import yaml
#
# from fabric.api import env, task
# from keystoneauth1 import loading
# from keystoneauth1 import session
# from novaclient.client import Client
#
#
# @task
# def prepare():
#     """Prepare Catalyst Cloud specific environment"""
#     env.buildparams['name'] = 'test-instance'
#     env.buildparams['root_user'] = 'ubuntu'
#
#
# @task
# def create_volume():
#     pass
#
#
# @task
# def create_server():
#     creds_path = env.buildparams['creds_filepath']
#     with open(creds_path, 'r') as creds_file:
#         creds = yaml.load(creds_file)
#
#     loader = loading.get_plugin_loader('password')
#     auth = loader.load_from_options(**creds)
#     sess = session.Session(auth=auth)
#
#     nova = Client('2', session=sess)
#     logging.info(creds)
#     nova.flavors.list()
