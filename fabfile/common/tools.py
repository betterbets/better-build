import logging
import subprocess
import sys
import time

from fabric.api import env, task

#TODO: fix these hard coded paths
HOSTS_TEMPLATE_PATH = 'playbooks/hosts_template'


class ConnectionException(Exception):
    pass


@task
def configure_ansible_hosts():
    """Configure the hosts file for an ansible playbook"""
    logging.info('Configuring ansible hosts file')
    hosts_path = env.buildparams['hosts_path']
    ip_address = env.buildparams['ip_address']

    hosts_template_path = '{}/{}'.format(
        env.buildparams['base_dir_path'],
        HOSTS_TEMPLATE_PATH
    )
    with open(hosts_template_path, 'r') as hosts_template:
        contents = hosts_template.read()

    with open(hosts_path, 'w') as hosts_file:
        hosts_file.write(contents.format(ip_address=ip_address))


@task
def check_connection():
    hostname = env.buildparams['name']
    ip_address = env.buildparams['ip_address']

    logging.info('Testing connection via SSH: {} ({})...'.format(
        hostname,
        ip_address,
    ))
    cmd = 'ssh -o ConnectTimeout=2 -o StrictHostKeyChecking=no  {}@{} hostname'.format(
        env.buildparams['root_user'],
        env.buildparams['ip_address']
    )

    successful = False
    for i in range(30):
        proc = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True
        )
        out, err = proc.communicate()
        if out == '{}\n'.format(hostname):
            logging.info('Connection to "{} ({})" successful!'.format(
                hostname,
                ip_address
            ))
            successful = True
            break
        else:
            time.sleep(2)
    if not successful:
        raise ConnectionException('Cannot connect to host')


@task
def configure_logging():
    logging.basicConfig(filename='build.log', level=logging.INFO)
    # configure stdout
    logging.StreamHandler(sys.stdout)
    std_out_handler = logging.StreamHandler(sys.stdout)
    std_out_handler.setLevel(logging.INFO)
    basic_logger = logging.getLogger()
    basic_logger.addHandler(std_out_handler)
    # disable other loggers
    logging.getLogger('urllib3').setLevel(logging.WARNING)
    logging.getLogger('swiftclient').setLevel(logging.WARNING)
