import logging

from fabric.api import env, execute, local, task


@task
def prepare():
    """Common environment variables for all builds"""
    execute('common.providers.build_{}.prepare'.format(
        env.buildparams['provider_name']
    ))


@task
def run_playbook(playbook_name):
    """Configure hosts and run an ansible playbook"""
    env.buildparams['playbook_path'] = '{}/playbooks/{}.yml'.format(
        env.buildparams['base_dir_path'],
        playbook_name,
    )
    env.buildparams['hosts_path'] = '{}/playbooks/hosts'.format(
        env.buildparams['base_dir_path']
    )

    execute('common.tools.configure_ansible_hosts')

    cmd = 'ansible-playbook -vv -i {} {}'.format(
        env.buildparams['hosts_path'],
        env.buildparams['playbook_path'],
    )
    logging.info('Running playbook: "{}"'.format(playbook_name))
    local(cmd)


@task
def build():
    """Common build execution

    1. Configures logging
    2. Prepare the environments
    3. Builds the servers
    4. Checks connection

    Provider retrieved from the environment, set in the specific build preparation
    """
    execute('common.tools.configure_logging')
    logging.info('---------------------------------')
    logging.info('Build Started!')
    logging.info('---------------------------------')
    provider_name = env.buildparams['provider_name']

    execute('common.providers.build_{}.create_server'.format(provider_name))
    execute('common.tools.check_connection')
    execute(run_playbook, 'base')
