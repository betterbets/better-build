from fabric.api import env, execute, task


HOSTNAME = 'better-ci'

@task
def prepare_ci_server():
    execute('build_digitalocean.prepare_digitalocean')
    env.buildparams['hostname'] = HOSTNAME
    env.buildparams['name'] = HOSTNAME
    env.buildparams['private_networking'] = True


@task(default=True)
def build_ci_server():
    """Build CI server on DigitalOcean

    1. Prepare DigitalOcean environment variables
    2. Prepare CI specific environment variables
    3. Build Server
    3. Run base
    4. Run CI playbook
    """
    execute(prepare_ci_server)
    execute('common.build.build')
    execute('common.build.run_playbook', 'ci_server')