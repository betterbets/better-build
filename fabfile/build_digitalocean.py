"""DigitalOcean build script

1. Prepares the digitalocean environment
2. Builds the server and records the IP address
3. Configures Ansible hosts with that ip address
4. Runs the digitalocean playbook
"""
from fabric.api import env, execute, task


PROVIDER_NAME = 'digitalocean'
HOSTNAME = 'digitalocean-server'
BASE_DIR_PATH = '/home/chris/Workspace/betterbets/better-build'
TOKEN_PATH = '/home/chris/Workspace/betterbets/secrets/token'
PRIVATE_NETWORKING = True


@task
def prepare_digitalocean():
    env.buildparams = {}

    env.buildparams['provider_name'] = PROVIDER_NAME
    env.buildparams['hostname'] = HOSTNAME
    env.buildparams['token_path'] = TOKEN_PATH
    env.buildparams['base_dir_path'] = BASE_DIR_PATH
    env.buildparams['private_networking'] = PRIVATE_NETWORKING

    execute('common.build.prepare')


@task(default=True)
def build_digitalocean():
    """Main build task"""
    execute(prepare_digitalocean)
    execute('common.build.build')
