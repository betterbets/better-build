from fabric.api import env, execute, task


HOSTNAME = 'better=vpn'

@task
def prepare_vpn():
    env.buildparams['private_networking'] = True
    env.buildparams['hostname'] = HOSTNAME

@task
def build_vpn():
    """Build VPN server on DigitalOcean

    1. Prepare DigitalOcean environment variables
    2. Prepare VPN specific environment variables
    3. Build Server
    4. Run VPN playbook
    """
    execute('build_digitalocean.prepare_digitalocean')
    execute(prepare_vpn)
    execute('build_digitalocean')
    execute('common.build.run_playbook', 'vpn')
